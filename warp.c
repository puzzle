// https://stackoverflow.com/a/20581721

#include <stdio.h>
#include <stdlib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

void mouseMove(int dx, int dy)
{
    Display *displayMain = XOpenDisplay(NULL);

    if(displayMain == NULL)
    {
        fprintf(stderr, "XOpenDisplay() failed\n");
        exit(EXIT_FAILURE);
    }

    XWarpPointer(displayMain, None, None, 0, 0, 0, 0, dx, dy);

    XCloseDisplay(displayMain);
}

int main(int argc, char **argv) {
  if (! (argc > 2)) return 1;
  mouseMove(atoi(argv[1]), atoi(argv[2]));
  return 0;
}

