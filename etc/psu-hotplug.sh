#!/bin/bash
ACTIVE="${1}"
VGAUSER=puzzle
XUSER="$(who | grep -F "(:0)" | cut -d\  -f 1)"
if [ "x${XUSER}" = "x${VGAUSER}" ]
then
  if [ "x${ACTIVE}" = "x0" ]
  then
    # power off after 10s if power is not restored
    sleep 10
    shutdown now -h
  else
    # power has been restored, cancel any pending power off
    killall -KILL psu-hotplug.sh
  fi
fi
