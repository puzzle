#!/bin/bash
VGAUSER=puzzle
XUSER="$(who | grep -F "(:0)" | cut -d\  -f 1)"
if [ "x${XUSER}" = "x${VGAUSER}" ]
then
  # tear down puzzle user session so it restarts cleanly
  killall --user puzzle
fi
