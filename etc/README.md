# system configuration

## udev rules for hotplug events

`/etc/udev/rules.d/`
: - `50-psu-hotplug.rules`
: - `50-vga-hotplug.rules`

## scripts launched by udev rules

`/usr/local/bin/`
: - `psu-hotplug.sh` shuts down if AC power is lost for more than 10 seconds
: - `vga-hotplug.sh` restarts Puzzle if VGA is unplugged or plugged in

## sudo configuration for cpu governor

`/etc/sudoers.d/puzzle.conf`
: - `sudoers.conf` allows puzzle user to set cpu governor to performance to avoid glitches

## auto login

`/etc/lightdm/`
: - `lightdm.conf` auto logs in puzzle user after 60 seconds, using fluxbox
