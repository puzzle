#!/bin/bash
#set -x
cd "$(dirname "$(readlink -f "${0}")")"
while true
do
# reduce pd watchdog messages in .xsession-errors
# requires sudoers configuration for no password
cat /proc/cpuinfo |
grep ^processor |
while read header separator processor
do
  sudo cpufreq-set -c "${processor}" -g performance
done
# detect screens
SCREENS="$(xrandr | grep ' connected ' | sed 's/ .*$//g')"
FPS="25"
INTERNAL="$(echo "${SCREENS}" | tr ' ' '\n' | grep    '^LVDS' | head -n 1 | tr -d '[:space:]' )"
EXTERNAL="$(echo "${SCREENS}" | tr ' ' '\n' | grep -v '^LVDS' | head -n 1 | tr -d '[:space:]' )"
SCREEN_OFF=""
if [ "x${EXTERNAL}" = "x" ]
then
  if [ "x${INTERNAL}" = "x" ]
  then
    echo "puzzle: no screens recognized"
    exit 1
  else
    echo "puzzle: using internal screen"
    SCREEN="${INTERNAL}"
  fi
else
  SCREEN="${EXTERNAL}"
  if [ "x${INTERNAL}" = "x" ]
  then
    echo "puzzle: using external screen"
  else
    echo "puzzle: using external screen (internal detected too)"
    SCREEN_OFF="${INTERNAL}"
  fi
fi
# set up screens
if [ "x${SCREEN_OFF}" = 'x' ]
then
  xrandr --output "${SCREEN}" --auto
else
  xrandr --output "${SCREEN}" --auto --output "${SCREEN_OFF}" --off
fi
xset -dpms s off
xsetroot -solid black -cursor_name none
# get screen resolution
RESOLUTION="$(xrandr | grep "^${SCREEN} connected " | sed "s/primary //g" | sed "s/^${SCREEN} connected \([^ ]*\) .*$/\1/")"
if [ "x${RESOLUTION}" = "x" ]
then
  echo "puzzle: failed to detect screen resolution"
  exit 1
fi
echo "${RESOLUTION}" | tr 'x+' '  ' | (
  read SCREENW SCREENH SCREENX SCREENY
  echo "resolution '$SCREENW' '$SCREENH' '$SCREENX' '$SCREENY'"
  # set framerate as high as possible
  xrandr --output "${SCREEN}" --mode "${SCREENW}x${SCREENH}" --rate 1000
  if (( SCREENH >= 2048 ))
  then
    echo "puzzle: screen height ${SCREENH} too large"
  elif (( SCREENH >= 1024 ))
  then
    SCREENY=$(( SCREENY + (SCREENH - 1024)/2 ))
    SCREENH=1024
  elif (( SCREENH >= 512 ))
  then
    SCREENY=$(( SCREENY + (SCREENH - 512)/2 ))
    SCREENH=512
  elif (( SCREENH >= 256 ))
  then
    SCREENY=$(( SCREENY + (SCREENH - 256)/2 ))
    SCREENH=256
  else
    echo "puzzle: screen height ${SCREENH} too small"
    exit 1
  fi
  if (( SCREENH > SCREENW ))
  then
    echo "puzzle: screen width ${SCREENW} smaller than height ${SCREENH}"
    exit 1
  else
    SCREENX=$(( SCREENX + (SCREENW - SCREENH)/2 ))
    SCREENW=$(( SCREENH ))
  fi
  # move mouse cursor to top right hand corner, and hide it
  ( while true ; do xsetroot -cursor blank.xbm blank.xbm ; ./warp 10000 -10000 ; sleep 10 ; done ) &
  # geometry of window centered on screen will be WxH+X+Y
  pd -noprefs -stderr -rt -nogui -noadc -r 48000 \
    -alsa -audiobuf 240 -outchannels 2 -path /home/puzzle/opt/lib/pd/extra -path /usr/lib/pd/extra/markex -lib Gem:pdlua -open puzzle/PUZZLE3X3.pd \
    -send "; PUZZLE ${SCREENW} ${SCREENH} ${SCREENX} ${SCREENY} ${DISPLAY} 0 ${FPS}"
)
echo "puzzle: done"
sleep 5
# restart from scratch if it crashes, to ensure clean setup
killall --user puzzle
done
