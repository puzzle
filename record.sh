#!/bin/bash
mkdir -p puzzle/out
rm -f puzzle/out/puzzle.ppm
mkfifo puzzle/out/puzzle.ppm
pd -noprefs -nostdpath -stderr -nrt -batch -nogui -noadc -nodac -r 48000 \
    -path ~/opt/Gem/lib/pd/extra/Gem -lib Gem \
    -path /usr/lib/pd/extra/markex \
    -path /usr/lib/pd/extra/pdlua -lib pdlua \
    -open puzzle/PUZZLE3X3.pd \
    -send "; PUZZLE 1024 1024 896 56 ${DISPLAY} 1 60" &
sleep 1
cd puzzle/out
ffmpeg -r 60 -framerate 60 -f image2pipe -i puzzle.ppm -pix_fmt yuv420p -profile:v high -level:v 5.1 -crf:v 20 puzzle.mkv &&
fdkaac -b 384k puzzle.wav &&
ffmpeg -i puzzle.mkv -i puzzle.m4a -codec:a copy -codec:v copy -movflags +faststart puzzle.mp4
cd ../..
ffprobe puzzle/out/puzzle.mp4
ls -1sh puzzle/out/puzzle.mp4
