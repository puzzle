local tswap = pd.Class:new():register("puzzle3x3-tswap")

function tswap:initialize(sel, atoms)
  self.inlets = 1
  self.outlets = 0
  return true
end

function tswap:in_1_symbol(s)
  local t = pd.Table:new():sync(s)
  if t ~= nil then
    local i
    local l = t:length()
    local n = 0
    for i = 1,l do
      local x = t:get(i-1)
      if x ~= 0 then
        n = n + 1
      end
    end
    local m = math.random(1, n)
    n = 0
    local a = math.random(1, l)
    local b = math.random(1, l)
    for i = 1,l do
      local x = t:get(i-1)
      if x ~= 0 then
        n = n + 1
      end
      if n == m then
        a = i
      end
    end
    local x = t:get(a-1)
    local y = t:get(b-1)
    t:set(a-1,y)
    t:set(b-1,x)
    t:redraw()
  end
end
