local P = pd.Class:new():register("puzzle3x3")

local function O()
  return {
    0,0,0,0,
    0,0,0,0,
    0,0,0,0,
    0,0,0,0
  }
end

local function I()
  return {
    1,0,0,0,
    0,1,0,0,
    0,0,1,0,
    0,0,0,1
  }
end

local function M(a,b)
  local c = O(), i, j, k
  for i = 0,3 do
    for j = 0,3 do
      for k = 0,3 do
        c[4 * i + j + 1] = c[4 * i + j + 1] + a[4 * i + k + 1] * b[4 * k + j + 1]
      end
    end
  end
  return c
end

local function translate(x, y) return function(t)
  return {
    1,0,0,x*t,
    0,1,0,y*t,
    0,0,1,0,
    0,0,0,1
  }
end end

local function rotate(x,y,z)
  local a, h, i, j, k, l
  if x ~= 0 then
    a = x
    h = 4 * 0 + 0 + 1
    i = 4 * 1 + 1 + 1
    j = 4 * 1 + 2 + 1
    k = 4 * 2 + 1 + 1
    l = 4 * 2 + 2 + 1
  else if y ~= 0 then
    a = y
    h = 4 * 1 + 1 + 1
    i = 4 * 0 + 0 + 1
    j = 4 * 0 + 2 + 1
    k = 4 * 2 + 0 + 1
    l = 4 * 2 + 2 + 1
  else if z ~= 0 then
    a = z
    h = 4 * 2 + 2 + 1
    i = 4 * 0 + 0 + 1
    j = 4 * 0 + 1 + 1
    k = 4 * 1 + 0 + 1
    l = 4 * 1 + 1 + 1
  end end end
  return function(t)
    local m, c, s, ll
    ll = 1
    if t == 1 then
      if a ==    0 then c =  1 ; s =  0 else
      if a ==   90 then c =  0 ; s =  1 else
      if a ==  -90 then c =  0 ; s = -1 else
      if a ==  180 then c = -1 ; s =  0 else
      if a == -180 then c = -1 ; s =  0 else
      if a ==   45 then c = math.sqrt(0.5) ; s =  math.sqrt(0.5) else
      if a ==  -45 then c = math.sqrt(0.5) ; s = -math.sqrt(0.5) else
      c = math.cos(a * math.pi / 180)
      s = math.sin(a * math.pi / 180)
      end end end end end end end
    else
      if a == 90 then
        ll = math.sqrt(0.5) / math.cos((t * 90 - 45) * math.pi / 180)
      else if a == -90 then
        ll = math.sqrt(0.5) / math.cos((t * 90 - 45) * math.pi / 180)
      end end
      c = math.cos(t * a * math.pi / 180)
      s = math.sin(t * a * math.pi / 180)
    end
    m = I()
    m[h] = ll
    m[i] = ll *  c
    m[j] = ll *  s
    m[k] = ll * -s
    m[l] = ll *  c
    return m
  end
end

local function deepen(t)
  local m
  m = I()
  m[12] = -0.5 * math.cos((t * 180 - 90) * math.pi / 180)
  return m
end

function P:initialize(atoms)
  self.inlets = 2
  self.outlets = 2
  self.x = 2
  self.y = 2
  self.tiles = {
    {  0,  1,  2 },
    {  7, "_", 3 },
    {  6,  5,  4 }
  }
  self.matrix = { }
  self.tweenL = { }
  self.tweenR = { }
  local x, y, t
  for y = 1,3 do
    for x = 1,3 do
      t = self.tiles[y][x]
      if t ~= "_" then
        self.matrix[t] = translate(x - 2, y - 2)(1)
        self.tweenL[t] = I
        self.tweenR[t] = I
      end
    end
  end
  return true
end

function P:in_2_N()
  if self.y > 1 then
    self:update()
    self.tweenL[self.tiles[self.y - 1][self.x]] = translate(0, 1)
    self.tiles[self.y][self.x] = self.tiles[self.y - 1][self.x]
    self.y = self.y - 1
    self.tiles[self.y][self.x] = "_"
  else
    self:outlet(2, "bang", { })
  end
end
function P:in_2_E()
  if self.x < 3 then
    self:update()
    self.tweenL[self.tiles[self.y][self.x + 1]] = translate(-1, 0)
    self.tiles[self.y][self.x] = self.tiles[self.y][self.x + 1]
    self.x = self.x + 1
    self.tiles[self.y][self.x] = "_"
  else
    self:outlet(2, "bang", { })
  end
end
function P:in_2_S()
  if self.y < 3 then
    self:update()
    self.tweenL[self.tiles[self.y + 1][self.x]] = translate(0, -1)
    self.tiles[self.y][self.x] = self.tiles[self.y + 1][self.x]
    self.y = self.y + 1
    self.tiles[self.y][self.x] = "_"
  else
    self:outlet(2, "bang", { })
  end
end
function P:in_2_W()
  if self.x > 1 then
    self:update()
    self.tweenL[self.tiles[self.y][self.x - 1]] = translate(1, 0)
    self.tiles[self.y][self.x] = self.tiles[self.y][self.x - 1]
    self.x = self.x - 1
    self.tiles[self.y][self.x] = "_"
  else
    self:outlet(2, "bang", { })
  end
end

function P:in_2_L(atoms)
  self:update()
  self.tweenR[atoms[1]] = rotate(0,0,90)
end
function P:in_2_R(atoms)
  self:update()
  self.tweenR[atoms[1]] = rotate(0,0,-90)
end

function P:in_2_NS(atoms)
  self:update()
  self.tweenR[atoms[1]] = rotate(180,0,0)
  self.tweenL[atoms[1]] = deepen
end
function P:in_2_SN(atoms)
  self:update()
  self.tweenR[atoms[1]] = rotate(-180,0,0)
  self.tweenL[atoms[1]] = deepen
end

function P:in_2_EW(atoms)
  self:update()
  self.tweenR[atoms[1]] = rotate(0,180,0)
  self.tweenL[atoms[1]] = deepen
end
function P:in_2_WE(atoms)
  self:update()
  self.tweenR[atoms[1]] = rotate(0,-180,0)
  self.tweenL[atoms[1]] = deepen
end
function P:in_2_NWSE(atoms)
  self:update()
  self.tweenR[atoms[1]] = function(t) return M(rotate(0,0,45)(1),M(rotate(0,180,0)(t),rotate(0,0,-45)(1))) end
  self.tweenL[atoms[1]] = deepen
end
function P:in_2_SENW(atoms)
  self:update()
  self.tweenR[atoms[1]] = function(t) return M(rotate(0,0,45)(1),M(rotate(0,-180,0)(t),rotate(0,0,-45)(1))) end
  self.tweenL[atoms[1]] = deepen
end
function P:in_2_NESW(atoms)
  self:update()
  self.tweenR[atoms[1]] = function(t) return M(rotate(0,0,-45)(1),M(rotate(0,180,0)(t),rotate(0,0,45)(1))) end
  self.tweenL[atoms[1]] = deepen
end
function P:in_2_SWNE(atoms)
  self:update()
  self.tweenR[atoms[1]] = function(t) return M(rotate(0,0,-45)(1),M(rotate(0,-180,0)(t),rotate(0,0,45)(1))) end
  self.tweenL[atoms[1]] = deepen
end

function P:in_1_float(z)
  local x, y, t, m
  for y = 1,3 do
    for x = 1,3 do
      t = self.tiles[y][x]
      if t ~= "_" then
        m = M(M(self.tweenL[t](z), self.matrix[t]), self.tweenR[t](z))
        self:outlet(1, "list", { t, "matrix", m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8], m[9], m[10], m[11], m[12], m[13], m[14], m[15], m[16] })
      end
    end
  end
end

function P:update()
  for t = 0,7 do
    self.matrix[t] = M(M(self.tweenL[t](1), self.matrix[t]), self.tweenR[t](1))
    self.tweenL[t] = I
    self.tweenR[t] = I
  end
end
