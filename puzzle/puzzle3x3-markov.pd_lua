local P = pd.Class:new():register("puzzle3x3-markov")

math.randomseed( tonumber(tostring(os.time()):reverse():sub(1,6)) )

function P:initialize(atoms)
  self.inlets = 1
  self.outlets = 1
  self.x = 2
  self.y = 2
  self.back = "_"
  self.moves = {
    { { "S", "E" }, { "W", "S", "E" }, { "W", "S" } },
    { { "N", "S", "E" }, { "N", "W", "S", "E" }, { "N", "W", "S" } },
    { { "N", "E" }, { "N", "W", "E" }, { "N", "W" } }
  }
  self.spin = { "L", "R" }
  self.flip = { "NS", "SN", "EW", "WE", "NWSE", "SENW", "NESW", "SWNE" }
  self.dx = { W = -1, E = 1, N = 0, S = 0 }
  self.dy = { W = 0, E = 0, N = -1, S = 1 }
  self.opposite = { W = "E", E = "W", N = "S", S = "N" }
  return true
end

function P:in_1_bang()
  local d, r, m
  r = math.random(1,16)
  if r == 1 then
    t = math.random(0,7)
    r = math.random(1,2)
    m = self.spin[r]
    self:outlet(1, m, { t })
  else if r == 2 then
    t = math.random(0,7)
    r = math.random(1,8)
    m = self.flip[r]
    self:outlet(1, m, { t })
  else while true do
    d = 2
    if self.x == 2 then d = d + 1 end
    if self.y == 2 then d = d + 1 end
    r = math.random(1, d)
    m = self.moves[self.y][self.x][r]
    if m ~= self.back then
      self.back = self.opposite[m]
      self.x = self.x + self.dx[m]
      self.y = self.y + self.dy[m]
      self:outlet(1, m, { })
      return
    end
  end end end
end
