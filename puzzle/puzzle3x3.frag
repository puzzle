uniform sampler2D tex;

void main (void)
{
  vec2 coord = (gl_TextureMatrix[0] * gl_TexCoord[0]).st;
  vec4 colour = texture2D(tex, coord) * gl_Color;
#if 1 // broken derivatives on old laptop
  float d = 1.0 / 64.0;
#else
  float d = length(vec4(dFdx(coord), dFdy(coord))) * sqrt(2.0);
#endif
  colour.a
    = smoothstep(0.0, d, coord.x)
    * smoothstep(0.0, d, 1.0 - coord.x)
    * smoothstep(0.0, d, coord.y)
    * smoothstep(0.0, d, 1.0 - coord.y)
    ;
  gl_FragColor = colour;
}
