LIBPD_DIR = ../../../
GLU_DIR = $(HOME)/opt

SRC_FILES = puzzle.c glu.c
TARGET = puzzle-em.html

CFLAGS = -I$(LIBPD_DIR)/pure-data/src -I$(LIBPD_DIR)/libpd_wrapper -O3
LDFLAGS = -L$(LIBPD_DIR)/libs -lpd -L$(GLU_DIR)/lib -lGLU Gem.bc pdlua.bc

$(TARGET): $(SRC_FILES) Makefile
	emcc $(CFLAGS) -o $(TARGET) $(SRC_FILES) \
	-DUSE_MGL_NAMESPACE=1 \
	-s USE_SDL=2 \
	-s USE_REGAL=1 \
	-s LEGACY_GL_EMULATION=0 \
	-s ERROR_ON_UNDEFINED_SYMBOLS=0 \
	--preload-file puzzle \
	--preload-file Gem \
	--preload-file pd.lua \
	$(LDFLAGS)
