#!/bin/bash
pd -noprefs -stderr -nrt \
    -path Gem -lib Gem:pdlua \
    -r 48000 -jack -channels 2 \
    -path ./puzzle/drums/ -open puzzle/PUZZLE3X3.pd \
    -send "; PUZZLE 256 256 1110 24 ${DISPLAY} 0 60"
