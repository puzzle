#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>

#include <emscripten.h>

#include <stdio.h>
#include "z_libpd.h"

extern void Gem_setup(void);
extern void pdlua_setup(void);

void audio(void *userdata, Uint8 *stream, int len)
{
  float inbuf[64][2], outbuf[64][2];
  float *b = (float *) stream;
  int m = len / sizeof(float) / 2;
  int k = 0;
  while (m > 0)
  {
    for (int i = 0; i < 64; ++i)
      for (int j = 0; j < 2; ++j)
        inbuf[i][j] = 0;
    libpd_process_float(1, &inbuf[0][0], &outbuf[0][0]);
    for (int i = 0; i < 64; ++i)
      for (int j = 0; j < 2; ++j)
        b[k++] = outbuf[i][j];
    m -= 64;
  }
  if (m < 0)
  {
    fprintf(stderr, "buffer overflow, m went negative: %d\n", m);
  }
}

void pdprint(const char *s) {
  printf("%s", s);
}

void main1(void)
{
  // nop
}

int main(int argc, char **argv)
{
  // initialize SDL2 audio
  SDL_Init(SDL_INIT_AUDIO);
  SDL_AudioSpec want, have;
  want.freq = 48000;
  want.format = AUDIO_F32;
  want.channels = 2;
  want.samples = 4096;
  want.callback = audio;
  SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_ANY_CHANGE);
  printf("AUDIO: want: %d %d %d %d\n", want.freq, want.format, want.channels, want.samples);
  printf("AUDIO: have: %d %d %d %d\n", have.freq, have.format, have.channels, have.samples);

  // initialize libpd
  libpd_set_printhook(pdprint);
  libpd_init();
  libpd_init_audio(2, 2, have.freq);

  // initialize Gem
  Gem_setup();
  pdlua_setup();

  // open patch
  libpd_openfile("PUZZLE3X3.pd", "puzzle");

  // startup message
  libpd_start_message(6);
  libpd_add_float(512); // size, must be power of 2
  libpd_add_float(512);
  libpd_add_float(0); // position, irrelevant for emscripten
  libpd_add_float(0);
  libpd_add_symbol(":0.0"); // display, irrelevant for emscripten
  libpd_add_float(0); // recording toggle
  libpd_finish_message("PUZZLE", "list");

  // start audio processing
  SDL_PauseAudioDevice(dev, 0);
  emscripten_set_main_loop(main1, 0, 1);
  return 0;
}

    // emscripten dl aborts; replace with graceful failure
    void *dlopen(const char *fn, int flags) { return 0; }
    int dlclose(void *handle) { return 0; }
    void *dlsym(void *h, const char *sym) { return 0; }
    char *dlerror(void) { return "not supported"; }
    
